var _ = require('lodash');
var q = require('q');


var contacts = [
        {id: '1', name: 'James Nelson', email: 'james@jamesknelson.com'},
        {id: '2', name: 'Joe Citizen', email: 'joe@example.com'},
        {id: '3', name: 'Jaime Torres', email: 'jatorres@vt.edu', address1: '1 Best Way Ln', city: 'Midlothian', state: 'VA', zip: '23112', phone: '555-123-1234', 
            description: 'Senior technology expert with fifteen years of experience building scalable SaaS platforms and web applications seeking opportunities as senior architect that allows me to use technology to support and grow the company.' }
];

var repo = {
    contacts: contacts,
    getAll: function() {
        var self = this;
        return q.fcall(function() { return self.contacts; });
    },
    get: function(id) {
        var self = this;
        var contact = _.find(self.contacts, function (o) { return o.id === id; });
        if (contact) {
            return q.fcall(function() { return contact; });
        }
        
        return q.fcall(function() { return null; });
    },
    upsert: function(contact) {
        var self = this;
        
        var idx = _.findIndex(this.contacts, {id: contact.id});
        if (idx === -1) {
            this.contacts.push(contact);
        } else {
            this.contacts[idx] = contact;
        }
        
        return q.fcall(function() { return contact; });
    },
    remove: function(id) {
        this.contacts = this.contacts.filter(function(o) {
            return o.id !== id; 
        });
        return q.fcall(function() { return true; });
    }
};

module.exports = repo;