var cassandra = require('cassandra-driver');
var q = require('q');
var client = new cassandra.Client({contactPoints: ['127.0.0.1'], keyspace: 'argo'});

var repo = {
    getAll: function() {
        var deferred = q.defer();
        var query = 'SELECT id, contact FROM contacts';
        
        client.execute(query, function (err, result) {
           if (!err){
               if ( result.rows.length > 0 ) {
                   var contacts = [];
                   for(var i = 0; i < result.rows.length; i++) {
                       var contact = result.rows[i].contact;
                       contact.id = result.rows[i].id;
                       contacts.push(contact);
                   }
                   
                   deferred.resolve(contacts);
               } else {
                   deferred.resolve([]);
               }
           } else {
               deferred.resolve([]);
           }
        });
        
        return deferred.promise;
    },
    
    get: function(id) {
        var deferred = q.defer();
        var query = 'SELECT id, contact FROM contacts WHERE id = ?';
        
        client.execute(query, [id], { prepare: true }, function (err, result) {
           if (!err){
               if ( result.rows.length > 0 ) {
                   var row = result.first();
                   var contact = row.contact;
                   contact.id = row.id;
                   
                   deferred.resolve(contact);
               } else {
                   deferred.resolve(null);
               }
           } else {
               deferred.resolve(null);
           }
        });
        
        return deferred.promise;
    },
    upsert: function(contact) {
        var record = Object.assign({}, contact);
        delete record.id;
        
        var deferred = q.defer();
        var query = "INSERT into contacts (id, contact) VALUES (?, ?)";
        client.execute(query, [contact.id, record], { prepare: true }, function(err, result) {
            if (err) {
                deferred.resolve(null);
                console.log(err);
            }
            else deferred.resolve(contact);
        });
        return deferred.promise;
    },
    remove: function(id) {
        var deferred = q.defer();
        var query = "DELETE FROM contacts WHERE id = ?";
        client.execute(query, [id], { prepare: true }, function(err, result) {
            if (err) deferred.resolve(false);
            else deferred.resolve(true);
        });
        
        return deferred.promise;
    }
};

module.exports = repo;