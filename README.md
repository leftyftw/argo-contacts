# README #

This is my submission for Argo CodeChallenge.  The requirements provided asked for a bootstrap/react/express/node/cassandra-based solution to a simple contact management application with search and CRUD capabilities.  

### What's here? ###

* app.js, routes/*, repo/*, and bin/* represent the server-side components.
* views/* and public/* represent the presentation, with public/javascripts/contacts.js and public/javascripts/components/* representing the react-y things.
* repo/contracts represents an "in-memory" repo whereas repo/contracts-cassandra represents a cassandra-backed storage.  Usage is easily modified by changing the require line in api.js.
* init_db.cql represents the CQL scripts required to initialize the keyspace and types/tables for the application. 

### How do I get set up? ###

* ASSUMPTION: Cassandra is installed, and cqlsh is in your path.
* ASSUMPTION: git is installed and in your path
* Clone repo: git clone https://bitbucket.org/leftyftw/argo-contacts.git
* cd argo-contacts; cqlsh -f ./init_db.cql
* npm install; npm start
* Browse to http://localhost:3000


### Who do I talk to? ###

* Interested in hiring the author?  Contact Jaime Torres at mr.jaime.torres@gmail.com