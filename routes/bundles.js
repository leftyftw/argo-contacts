var express = require("express");
var browserify = require("browserify-middleware");
var router = express.Router();

browserify.settings({
  transform: ['reactify'],
  extensions: ['.js', '.jsx'],
  grep: /\.jsx?$/
});

router.get("/app.js", browserify("./public/javascripts/contacts.js"));

module.exports = router;



