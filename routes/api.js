var express = require('express');
var contactsRepo = require('../repo/contacts-cassandra');
var router = express.Router();

/* CONTACTS */

/* GET contacts listing. */
router.route('/contacts')
    .get(function(req, res, next) {
        
        contactsRepo.getAll().then(function(result) {
           res.json(result); 
        });
    })
    .post(function(req, res, next) {
        var contact = req.body;
        console.log("Insert: ", contact);
        
        contactsRepo.upsert(contact).then(function(result){
            if (result) res.status(200).json(result);
            else res.status(404).end();
        });
    });

router.route('/contacts/:id')
    .get(function(req, res, next) {
        contactsRepo.get(req.params.id).then(function(contact) {
            if (contact) {
                res.status(200).json(contact);
            } else {
                res.status(404).end();
            } 
        });
        
    })
    .delete(function(req, res, next) {
        contactsRepo.remove(req.params.id).then(function() {
            res.status(200).end();
        });        
    })
    .put(function(req, res, next) {
        var contact = req.body;
        console.log("Upsert: ", contact);
        contact.id = req.params.id;    
        contactsRepo.upsert(contact).then(function(result) {
            if (result) {
                res.status(200).json(result);
            } else {
                return res.status(404).end();
            }
        });
        
    });

module.exports = router;
