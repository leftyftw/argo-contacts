module.exports = ((function() {
    return {
      getAllContacts: function() {
          return fetch('/api/contacts').then(function(response) {
              return response.json();
          });
      },
      updateContact: function(contact) {
          if (!contact.id) throw new Error('Contact in invalid state!');
          return fetch('/api/contacts/' + contact.id, {
              method: 'PUT',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
              },
              body: JSON.stringify(contact)
          }).then(function(response) {
              if (response.status === 200) {
                  return response.json();
              }
              
              return null;
          });          
      },
      deleteContact: function(id) {
          if (!id) throw new Error('Invalid id!');
          return fetch('/api/contacts/' + id, {
              method: 'DELETE'
          });
      }
    };
}))();