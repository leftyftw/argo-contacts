var ContactView = require('./components/contact-view.jsx');
var contactApi = require('./api.js');
var util = require('./utilities.js');
               
var state = {};

var bllc = {
    saveContact: function() {
        var contacts = state.contacts;
        var contact = state.modalContact;
        
        var req = contactApi.updateContact(contact);
        
        req.then(function(contact) {
            if (contact) {
                contacts = _.unionBy([contact], contacts, 'id');
                setState({contacts: contacts});
            }
        });
        
        setState({modalContact: {}, showModal: false});
    },

    removeContact: function(id) {
        contactApi.deleteContact(id);
        var contacts = state.contacts.filter(function(o) { return o.id !== id; });
        setState({contacts: contacts});
    },

    editContact: function(contact) {
        setState({ modalContact: contact, showModal: true });
    },
    
    updateSearchText: function(searchText) {
        setState({ searchText: searchText});
    },
};

var modalOperations = {
    updateModalContact: function (contact) {
        setState({ modalContact: contact });
    },

    showContactModal: function(display, contact) {
        if (display) {
            setState({ modalContact: contact || {id: util.generateUUID() }, showModal: true });
        } else {
            setState({showModal: false});
        }
    },
};

function setState(changes) {
    Object.assign(state, changes);
    ReactDOM.render(
        React.createElement(ContactView, Object.assign({}, state, {
            onContactChange: modalOperations.updateModalContact,
            onShowContactModal: modalOperations.showContactModal,
            onSaveContact: bllc.saveContact,
            onRemoveContact: bllc.removeContact,
            onEditContact: bllc.editContact,
            onUpdateSearchText: bllc.updateSearchText
        })),
        
        document.getElementById('react-app')
    );
}

// Set initial data
setState({
  contacts: [],
  modalContact: {},
  showModal: false,
  searchText: ''
});

// Get initial data from server
contactApi.getAllContacts().then(function(contacts) {
    setState({contacts: contacts});
});