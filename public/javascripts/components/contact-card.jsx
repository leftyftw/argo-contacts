var md5 = require("../md5.js");

module.exports = React.createClass({
    propTypes: {
        contact: React.PropTypes.object.isRequired,
        onRemoveContact: React.PropTypes.func.isRequired,
        onEditContact: React.PropTypes.func.isRequired
    },
       
    render: function() {
        
        var Jumbotron = ReactBootstrap.Jumbotron;
        var Button = ReactBootstrap.Button;
        
        var address;
        if (this.props.contact.address1 || this.props.contact.city || this.props.contact.state || this.props.contact.zip ) {
            if (this.props.contact.address2) {
                address = <address>{this.props.contact.address1} <br/>{this.props.contact.address2} <br/> {this.props.contact.city}, {this.props.contact.state} {this.props.contact.zip}</address>
            }
            else {
                address = <address>{this.props.contact.address1} <br/> {this.props.contact.city}, {this.props.contact.state} {this.props.contact.zip}</address>
            }
        }
        
        var phone;
        if (this.props.contact.phone) {
            phone = <div><abbr title="Phone">P:</abbr> <a href={'tel:' + this.props.contact.phone}>{this.props.contact.phone}</a></div>;
        }
        
        var notes;
        if (this.props.contact.notes) {
            notes = <div>
                            <h4>Notes:</h4>
                            <div><em>{this.props.contact.notes}</em></div>
                        </div>;
        }
        
        var self = this;
        var deleteContact = function() {
            self.props.onRemoveContact(self.props.contact.id);
        };
        
        var editContact = function() {
            self.props.onEditContact(self.props.contact);
        };
        
        return (
            <div className='contact-card'>
                <Jumbotron className='container-fluid'>       
                    <div className='row'>
                        <div className='container-fluid'>
                            <div className='col-xs-5 col-md-4'>
                                <img className='contact-avatar img-responsive' src={'http://www.gravatar.com/avatar/' + md5(this.props.contact.email.trim().toLowerCase()) + '?s=200'}></img>
                            </div>
                            <div className='col-xs-7 col-md-4 col-lg-3'>
                                <h2> {this.props.contact.name} </h2>
                                {address}
                                {phone}
                                <a href={'mailto:' + this.props.contact.email}>{this.props.contact.email}</a>                           
                            </div>
                            <div className="col-xs-12 col-md-4 col-lg-5">
                                {notes}
                            </div>
                        </div>
                    </div>
                    <div className='row'>     
                        <div className='button-group pull-right'>
                            <Button title='Edit Contact' bsStyle='success' onClick={editContact}><i className='glyphicon glyphicon-edit'></i></Button>
                            <Button title='Delete Contact' bsStyle='danger' onClick={deleteContact}><i className='glyphicon glyphicon-trash'></i></Button>
                        </div>               
                    </div>
                </Jumbotron>
            </div>
        );
    }
});