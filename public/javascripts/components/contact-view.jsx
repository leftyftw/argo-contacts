var ContactCard = require("./contact-card.jsx");
var ContactInput = require("./contact-input.jsx");

module.exports = React.createClass({
  propTypes: {
    contacts: React.PropTypes.array.isRequired,
    searchText: React.PropTypes.string,
    modalContact: React.PropTypes.object.isRequired,
    showModal: React.PropTypes.bool.isRequired,
    onContactChange: React.PropTypes.func.isRequired,
    onSaveContact: React.PropTypes.func.isRequired,
    onShowContactModal: React.PropTypes.func.isRequired,
    onRemoveContact: React.PropTypes.func.isRequired,
    onEditContact: React.PropTypes.func.isRequired,
    onUpdateSearchText: React.PropTypes.func.isRequired
  },

  searchChanged: function(input) {
        this.props.onUpdateSearchText(input.target.value);
  },

  render: function() {
    var self = this;

    var showModal = function() {
        self.props.onShowContactModal(true);
    };
    
    var hideModal = function() {
        self.props.onShowContactModal(false);
    };
    
    var filterContacts = function() {
        return self.props.contacts
                .filter(function(c) {
                    if (!c.email) return false;
                    
                    var search = self.props.searchText;
                    
                    if (!search) return true;
                    return c.email.indexOf(search) !== -1 
                            || (c.name && c.name.indexOf(search) !== -1)
                            || (c.phone && c.phone.indexOf(search) !== -1);
                })
    }
    
    var Modal = ReactBootstrap.Modal;
    var Button = ReactBootstrap.Button;

    return (
        <div className='contact-view'>
            <div className='container-fluid'>
                <h1 className='ContactView-title col-xs-12 col-sm-6 col-lg-8'>Contacts</h1>
            <div className='col-xs-12 col-sm-6 col-lg-4'>
                    <div className='header-search'>
                        <div id="txt-search-contact">
                            <input className="form-control" type="text" placeholder="Search..." value={this.props.searchText} onChange={this.searchChanged}></input>
                        </div>
                        <Button bsStyle="primary" id="btn-add-contact" title='Add Contact' onClick={showModal}> <i className='glyphicon glyphicon-plus' /> <i className='glyphicon glyphicon-user' /></Button>
                    </div>
                </div>
            </div>
            
            {   
                filterContacts()
                .map(function(c) {
                    return <ContactCard key={c.id} contact={c} onRemoveContact={self.props.onRemoveContact} onEditContact={self.props.onEditContact} />;
            })}
            <Modal bsSize='lg' show={this.props.showModal} onHide={hideModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Add Contact</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ContactInput contact={this.props.modalContact} onChange={this.props.onContactChange} />
                </Modal.Body>
                <Modal.Footer>
                    <Button bsStyle='primary' onClick={this.props.onSaveContact}>Save</Button>
                    <Button onClick={hideModal}>Close</Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
  },
});