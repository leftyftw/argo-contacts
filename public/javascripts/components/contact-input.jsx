var md5 = require("../md5.js");

module.exports = React.createClass({
    propTypes: {
        contact: React.PropTypes.object.isRequired,
        onChange: React.PropTypes.func.isRequired
    },
    
    handleChange: function(field, e) {
        var update = {};
        update[field] = e.target.value;
        this.props.onChange(Object.assign({}, this.props.contact, update));
    },
    
    render: function() {
        
        var Jumbotron = ReactBootstrap.Jumbotron;
        
        var photoSource;
        if (this.props.contact.email) {
            photoSource = "http://www.gravatar.com/avatar/" + md5(this.props.contact.email.trim().toLowerCase()) + "?s=200";
        }
                
        return (
            <div className='contact-input'>
                <Jumbotron>
                    <div className="row">
                        <form>
                        <div className="col-xs-4">
                            <img src={photoSource}></img>
                        </div>
                        <div className="col-xs-4">
                            <div className="form-group">
                                <label className="sr-only" htmlFor="new-contact-name">Name</label>
                                <input className="col-xs-12 form-control" type="text" placeholder="Name" id="new-contact-name" value={this.props.contact.name} onChange={this.handleChange.bind(this, "name")}></input>
                            </div>    
                            <div className="form-group">
                                <label className="sr-only" htmlFor="new-contact-address1">Address 1</label>
                                <input className="col-xs-12 form-control" type="text" placeholder="Address 1" id="new-contact-address1" value={this.props.contact.address1} onChange={this.handleChange.bind(this, "address1")}></input>
                            </div>
                            <div className="form-group">
                                <label className="sr-only" htmlFor="new-contact-address2">Address 2</label>
                                <input className="col-xs-12 form-control" type="text" placeholder="Address 2" id="new-contact-address2" value={this.props.contact.address2} onChange={this.handleChange.bind(this, "address2")}></input>
                            </div>
                            <div className="form-group">
                            
                                <div className="nopad col-xs-6">
                                    <label className="sr-only" htmlFor="new-contact-city">City</label>
                                    <input className="form-control" type="text" placeholder="City" id="new-contact-city" value={this.props.contact.city} onChange={this.handleChange.bind(this, "city")}></input>
                                </div>
                                <div className="nopad col-xs-3">
                                    <label className="sr-only" htmlFor="new-contact-state">State</label>
                                    <input className="form-control" type="text" placeholder="State" id="new-contact-state" value={this.props.contact.state} onChange={this.handleChange.bind(this, "state")}></input>
                                </div>
                                <div className="nopad col-xs-3">
                                    <label className="sr-only" htmlFor="new-contact-zip">Zip Code</label>
                                    <input className="form-control" type="text" placeholder="Zip" id="new-contact-zip" value={this.props.contact.zip} onChange={this.handleChange.bind(this, "zip")}></input>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="sr-only" htmlFor="new-contact-phone">Phone</label>
                                <input className="col-xs-12 form-control" type="text" placeholder="Phone" id="new-contact-phone" value={this.props.contact.phone} onChange={this.handleChange.bind(this, "phone")}></input>
                            </div>
                            <div className="form-group">
                                <label className="sr-only" htmlFor="new-contact-email">E-mail</label>
                                <input type="text" className="col-xs-12 form-control" placeholder="E-mail" id="new-contact-email" value={this.props.contact.email} onChange={this.handleChange.bind(this, "email")}></input>
                            </div>                                                       
                        </div>
                        <div className="col-xs-4">
                            <div className="form-group">
                                <div><label className="sr-only" htmlFor="new-contact-description">Notes</label></div>
                                <textarea className="col-xs-12 form-control" placeholder="Notes" id="new-contact-description" rows="5" value={this.props.contact.notes} onChange={this.handleChange.bind(this, "notes")}></textarea>
                            </div>
                        </div>
                        </form>
                    </div>
                </Jumbotron>
            </div>
        );
    }
});